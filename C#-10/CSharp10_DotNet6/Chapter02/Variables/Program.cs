﻿// See https://aka.ms/new-console-template for more information

object height = 1.88;	// storing a double in an object
object name = "Amir";	// storing a string in an object
Console.WriteLine ($"{name} is {height} metres tall");

// int length1 = name.Length;	// gives compile error
int length2 = ((string) name).Length;
Console.WriteLine ($"{name} has {length2} characters.");

// storing a string in a dynamic object
dynamic something = "Sherman";

Console.WriteLine($"Length is {something.Length}.");

Console.WriteLine ("\r\nLet's print some default values for each type...");

Console.WriteLine ($"default(int) = {default(int)}");
Console.WriteLine ($"default(bool) = {default(bool)}");
Console.WriteLine ($"default(DateTime) = {default(DateTime)}");
Console.WriteLine ($"default(string) = {default(string)}");

int number = 13;
Console.WriteLine ($"number has been set to: {number}");
number = default;
Console.WriteLine($"number has been reset to the default: {number}");

Console.WriteLine("\r\nWorking with arrays...");

string [] names;

// allocating memory for four strings in an array
names = new string[4];

// storing items at index positions
names[0] = "Kate";
names[1] = "Jack";
names[2] = "Rebecca";
names[3] = "Tom";

// looping through the names
for ( int i = 0; i < names.Length; i++ ) {
	Console.WriteLine (names[i]);
}