﻿// See https://aka.ms/new-console-template for more information

using CallStackExceptionHandlingLib;
using static System.Console;

WriteLine("In Main");
Alpha ()
	;
static void Alpha () {
	WriteLine("In Alpha");
	Beta ();
}

static void Beta () {
	WriteLine("In Beta");
	try {
		Calculator.Gamma ();
	} catch ( IOException ex ) {
		// throw the caught exception as if it happened here
		// this will lose the original call stack
		// throw ex;
		
		// rethrow the caught exception and retain the original call stack
		// throw;
		
		// throw a new exception with the caught exception nested within it
		// throw new InvalidOperationException(
		// 	message: "Calculation had invalid values. See inner exception for why.",
		// 	innerException: ex);
		WriteLine($"Caught this: {ex.Message}");
		throw ex;
	}
}