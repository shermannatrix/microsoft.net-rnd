namespace PacktLibrary;

public partial class Person {
    public string Origin {
        get {
            return $"{Name} was born on {HomePlanet}";
        }
    }

    public string Greeting => $"{Name} says 'Hello!'";

    public int Age => System.DateTime.Today.Year - DateOfBirth.Year;

    public string FavouriteIceCream { get; set; }

    private string favouritePrimaryColor;

    public string FavouritePrimaryColor {
        get {
            return favouritePrimaryColor;
        }
        set {
            switch(value.ToLower()) {
                case "red":
                case "green":
                case "blue":
                    favouritePrimaryColor = value;
                    break;
                default:
                throw new System.ArgumentException(
                    $"{value} is not a primary color. Choose from: red, blue, green.");
            }
        }
    }

    public Person this[int index] {
        get {
            return Children[index];
        }
        set {
            Children[index] = value;
        }
    }
}