﻿using static System.Console;

string[] names = new[] { "Michael", "Pam", "Jim", "Dwight", "Angela", "Kevin", "Toby", "Creed" };

WriteLine("Deferred execution");

var query1 = names.Where(name => name.EndsWith("m"));

// written using LINQ query comprehension syntax
var query2 = from name in names where name.EndsWith("m") select name;

// Answer returned as an array of strings containing Pam and Jim
string[] result1 = query1.ToArray();

// Answer returned as a list of strings containing Pam and Jim
List<string> result2 = query2.ToList();

// Answer returned as we enumerate over the results
foreach (string name in query1)
{
	WriteLine(name);	// outputs Pam
	names[2] = "Jimmy";	// change Jim to Jimmy
}

// var query = names.Where(NameLongerThanFour);
// var query = names.Where(name => name.Length > 4);
IOrderedEnumerable<string> query = names
	.Where(name => name.Length > 4)
	.OrderBy(name => name.Length)
	.ThenBy(name => name);

foreach (string item in query)
{
	WriteLine(item);
}

WriteLine("Filtering by type");

static bool NameLongerThanFour(string name)
{
	return name.Length > 4;
}

WriteLine("Filtering by type");

List<Exception> exceptions = new()
{
	new ArgumentException(),
	new SystemException(),
	new IndexOutOfRangeException(),
	new InvalidOperationException(),
	new NullReferenceException(),
	new InvalidCastException(),
	new OverflowException(),
	new DivideByZeroException(),
	new ApplicationException()
};

IEnumerable<ArithmeticException> arithmeticExceptionsQuery = exceptions.OfType<ArithmeticException>();

foreach (ArithmeticException exception in arithmeticExceptionsQuery)
{
	WriteLine(exception);
}