﻿// See https://aka.ms/new-console-template for more information
using static System.Console;

int x = 0;

while (x < 10) {
	WriteLine(x);
	x++;
}

string? password;

do {
	Write("Enter your password:");
	password = ReadLine();
} while (password != "Pa55w0rd!");

WriteLine("Correct!");

// For loop
for (int y = 1; y <= 10; y++) {
	WriteLine(y);
}

// Foreach loop
string[] names = { "Adam", "Barry", "Charlie" };

foreach (string name in names) {
	WriteLine($"{name} has {name.Length} characters.");
}