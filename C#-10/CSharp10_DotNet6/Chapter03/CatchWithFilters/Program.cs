﻿// See https://aka.ms/new-console-template for more information
using static System.Console;

Write ( "Enter an amount: " );
string? amount = ReadLine ();

try {
	decimal amountValue = decimal.Parse ( amount );
} catch ( FormatException ) when ( amount.Contains ( "$" ) ) {
	WriteLine ( "Amounts cannot use the dollar sign!" );
} catch ( FormatException ) {
	WriteLine("Amounts must only contain digits!");
}